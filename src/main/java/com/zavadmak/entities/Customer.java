package com.zavadmak.entities;

import jakarta.persistence.*;

@Entity
@PrimaryKeyJoinColumn(name = "userId")
@DiscriminatorValue("customer")
public class Customer extends UserBase {

    public Customer() {
        super("customer");
    }

    public Customer(String fullName, String birthday, String login, String password, Integer psc, String town, String street) {
        this();
        this.setFullName(fullName);
        this.setBirthday(birthday);
        this.setLogin(login);
        this.setPassword(password);
        this.psc = psc;
        this.town = town;
        this.street = street;
    }

    @Basic
    @Column(name = "psc")
    private int psc;

    public int getPsc() {
        return psc;
    }

    public void setPsc(int psc) {
        this.psc = psc;
    }

    @Basic
    @Column(name = "town")
    private String town;

    public String getTown() {
        return town;
    }

    public void setTown(String town) {
        this.town = town;
    }

    @Basic
    @Column(name = "street")
    private String street;

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Customer customer = (Customer) o;

        if (getUserId() != customer.getUserId()) return false;
        if (psc != customer.psc) return false;
        if (town != null ? !town.equals(customer.town) : customer.town != null) return false;
        if (street != null ? !street.equals(customer.street) : customer.street != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = getUserId();
        result = 31 * result + psc;
        result = 31 * result + (town != null ? town.hashCode() : 0);
        result = 31 * result + (street != null ? street.hashCode() : 0);
        return result;
    }
}
