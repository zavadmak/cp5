package com.zavadmak.entities;

import jakarta.persistence.*;

import java.sql.Timestamp;

@Entity
public class Productitem {
    @Basic
    @jakarta.persistence.Column(name = "productitemid")
    private int productitemid;

    public int getProductitemid() {
        return productitemid;
    }

    public void setProductitemid(int productitemid) {
        this.productitemid = productitemid;
    }

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "productionnumber")
    private String productionnumber;

    public String getProductionnumber() {
        return productionnumber;
    }

    public void setProductionnumber(String productionnumber) {
        this.productionnumber = productionnumber;
    }

    @Basic
    @Column(name = "productid")
    private int productid;

    public int getProductid() {
        return productid;
    }

    public void setProductid(int productid) {
        this.productid = productid;
    }

    @Basic
    @Column(name = "price")
    private double price;

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Basic
    @Column(name = "manufacturedat")
    private Timestamp manufacturedat;

    public Timestamp getManufacturedat() {
        return manufacturedat;
    }

    public void setManufacturedat(Timestamp manufacturedat) {
        this.manufacturedat = manufacturedat;
    }

    @Basic
    @Column(name = "storekeeperid")
    private int storekeeperid;

    public int getStorekeeperid() {
        return storekeeperid;
    }

    public void setStorekeeperid(int storekeeperid) {
        this.storekeeperid = storekeeperid;
    }

    @Basic
    @Column(name = "addedat")
    private Timestamp addedat;

    public Timestamp getAddedat() {
        return addedat;
    }

    public void setAddedat(Timestamp addedat) {
        this.addedat = addedat;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Productitem that = (Productitem) o;

        if (productitemid != that.productitemid) return false;
        if (productid != that.productid) return false;
        if (Double.compare(price, that.price) != 0) return false;
        if (storekeeperid != that.storekeeperid) return false;
        if (productionnumber != null ? !productionnumber.equals(that.productionnumber) : that.productionnumber != null)
            return false;
        if (manufacturedat != null ? !manufacturedat.equals(that.manufacturedat) : that.manufacturedat != null)
            return false;
        if (addedat != null ? !addedat.equals(that.addedat) : that.addedat != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = productitemid;
        result = 31 * result + (productionnumber != null ? productionnumber.hashCode() : 0);
        result = 31 * result + productid;
        temp = Double.doubleToLongBits(price);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + (manufacturedat != null ? manufacturedat.hashCode() : 0);
        result = 31 * result + storekeeperid;
        result = 31 * result + (addedat != null ? addedat.hashCode() : 0);
        return result;
    }
}
