package com.zavadmak.entities;

import com.zavadmak.utils.DateParser;
import jakarta.persistence.*;

import java.sql.Date;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@DiscriminatorColumn(name = "userType")
public abstract class UserBase {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "userid", updatable = false)
    private int userId;
    @Basic
    @Column(name = "login", updatable = false)
    private String login;
    @Basic
    @Column(name = "password")
    private String password;
    @Basic
    @Column(name = "fullname")
    private String fullName;
    @Basic
    @Column(name = "birthday")
    private Date birthday;

    @Basic
    @Column(name = "userType")
    private String type;

    public UserBase(String type) {
        this.type = type;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = DateParser.dateOf(birthday);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UserBase userbase = (UserBase) o;

        if (userId != userbase.userId) return false;
        if (login != null ? !login.equals(userbase.login) : userbase.login != null) return false;
        if (password != null ? !password.equals(userbase.password) : userbase.password != null) return false;
        if (fullName != null ? !fullName.equals(userbase.fullName) : userbase.fullName != null) return false;
        if (birthday != null ? !birthday.equals(userbase.birthday) : userbase.birthday != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = userId;
        result = 31 * result + (login != null ? login.hashCode() : 0);
        result = 31 * result + (password != null ? password.hashCode() : 0);
        result = 31 * result + (fullName != null ? fullName.hashCode() : 0);
        result = 31 * result + (birthday != null ? birthday.hashCode() : 0);
        return result;
    }
}
