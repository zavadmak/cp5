package com.zavadmak.entities;

import jakarta.persistence.*;

import java.util.Collection;
import java.util.List;

@Entity
public class Manufacturer {

    public Manufacturer() {

    }

    public Manufacturer(String companyName) {
        this.companyName = companyName;
    }

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "manufacturerId")
    private int manufacturerId;

    @Column(name = "companyName")
    private String companyName;

    @ManyToMany(targetEntity = Product.class, fetch = FetchType.LAZY)
    @JoinTable(
            name = "productHasManufacturer",
            joinColumns = @JoinColumn(name = "manufacturerId"),
            inverseJoinColumns = @JoinColumn(name = "productId")
    )
    private List<Product> products;

    public List<Product> getProducts() {
        return products;
    }

    public int getManufacturerId() {
        return manufacturerId;
    }

    public void setManufacturerId(int manufacturerId) {
        this.manufacturerId = manufacturerId;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Manufacturer that = (Manufacturer) o;

        if (manufacturerId != that.manufacturerId) return false;
        if (companyName != null ? !companyName.equals(that.companyName) : that.companyName != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = manufacturerId;
        result = 31 * result + (companyName != null ? companyName.hashCode() : 0);
        return result;
    }
}
