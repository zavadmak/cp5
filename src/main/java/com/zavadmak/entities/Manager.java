package com.zavadmak.entities;

import jakarta.persistence.*;

import java.util.List;

@Entity
@PrimaryKeyJoinColumn(name = "userId")
@DiscriminatorValue("manager")
public class Manager extends UserBase {
    @Basic
    @Column(name = "education")
    private String education;

    @OneToMany(targetEntity = Checkout.class, mappedBy = "manager")
    private List<Checkout> checkouts;

    public List<Checkout> getCheckouts() {
        return checkouts;
    }

    public Manager() {
        super("manager");
    }
    public Manager(String fullName, String birthday, String login, String password, String education){
        this();
        this.setFullName(fullName);
        this.setBirthday(birthday);
        this.setLogin(login);
        this.setPassword(password);
        this.education = education;
    }

    public String getEducation() {
        return education;
    }

    public void setEducation(String education) {
        this.education = education;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Manager manager = (Manager) o;

        if (education != null ? !education.equals(manager.education) : manager.education != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = this.getUserId();
        result = 31 * result + (education != null ? education.hashCode() : 0);
        return result;
    }
}
