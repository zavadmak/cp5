package com.zavadmak.entities;

import jakarta.persistence.*;

import java.sql.Timestamp;

@Entity
public class Receipt {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @jakarta.persistence.Column(name = "receiptid")
    private int receiptid;

    public int getReceiptid() {
        return receiptid;
    }

    public void setReceiptid(int receiptid) {
        this.receiptid = receiptid;
    }

    @Basic
    @Column(name = "checkoutid")
    private int checkoutid;

    public int getCheckoutid() {
        return checkoutid;
    }

    public void setCheckoutid(int checkoutid) {
        this.checkoutid = checkoutid;
    }

    @Basic
    @Column(name = "createdat")
    private Timestamp createdat;

    public Timestamp getCreatedat() {
        return createdat;
    }

    public void setCreatedat(Timestamp createdat) {
        this.createdat = createdat;
    }

    public enum ReceiptType {
        RETURN,
        SELL
    }

    @Basic
    @Column(name = "receipttype")
    @Enumerated(value = EnumType.STRING)
    private ReceiptType receiptType;

    public ReceiptType getReceiptType() {
        return receiptType;
    }

    public void setReceiptType(ReceiptType receiptType) {
        this.receiptType = receiptType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Receipt receipt = (Receipt) o;

        if (receiptid != receipt.receiptid) return false;
        if (checkoutid != receipt.checkoutid) return false;
        if (createdat != null ? !createdat.equals(receipt.createdat) : receipt.createdat != null) return false;
        if (receiptType != null ? !receiptType.equals(receipt.receiptType) : receipt.receiptType != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = receiptid;
        result = 31 * result + checkoutid;
        result = 31 * result + (createdat != null ? createdat.hashCode() : 0);
        result = 31 * result + (receiptType != null ? receiptType.hashCode() : 0);
        return result;
    }
}
