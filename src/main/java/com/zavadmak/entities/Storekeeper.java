package com.zavadmak.entities;

import jakarta.persistence.*;

@Entity
@PrimaryKeyJoinColumn(name = "userId")
@DiscriminatorValue("storekeeper")
public class Storekeeper extends UserBase {
    @Basic
    @Column(name = "position")
    private String position;

    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinTable(name="storekeeperSupervisor",
            joinColumns = @JoinColumn(name = "storekeeperId"),
            inverseJoinColumns = @JoinColumn(name = "storekeeperIdSupervisor")
    )
    private Storekeeper supervisor;

    public Storekeeper() {
        super("storekeeper");
    }
    public Storekeeper(String fullName, String birthday, String login, String password, String position) {
        this();
        this.setFullName(fullName);
        this.setBirthday(birthday);
        this.setLogin(login);
        this.setPassword(password);
        this.position = position;
    }

    public void setSupervisor(Storekeeper supervisor) {
        this.supervisor = supervisor;
    }

    public Storekeeper getSupervisor() {
        return supervisor;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Storekeeper that = (Storekeeper) o;

        if (getUserId() != that.getUserId()) return false;
        if (position != null ? !position.equals(that.position) : that.position != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = getUserId();
        result = 31 * result + (position != null ? position.hashCode() : 0);
        return result;
    }
}
