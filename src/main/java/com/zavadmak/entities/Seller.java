package com.zavadmak.entities;

import jakarta.persistence.*;

@Entity
@PrimaryKeyJoinColumn(name = "userId")
@DiscriminatorValue("seller")
public class Seller extends UserBase {

    @Basic
    @Column(name = "internalphone")
    private String internalPhone;


    public Seller() {
        super("seller");
    }

    public Seller(String fullName, String birthday, String login, String password, String internalPhone) {
        this();
        this.setFullName(fullName);
        this.setBirthday(birthday);
        this.setLogin(login);
        this.setPassword(password);
        this.internalPhone = internalPhone;
    }

    public String getInternalPhone() {
        return internalPhone;
    }

    public void setInternalPhone(String internalPhone) {
        this.internalPhone = internalPhone;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Seller seller = (Seller) o;

        if (this.getUserId() != seller.getUserId()) return false;
        if (internalPhone != null ? !internalPhone.equals(seller.internalPhone) : seller.internalPhone != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = this.getUserId();
        result = 31 * result + (internalPhone != null ? internalPhone.hashCode() : 0);
        return result;
    }
}
