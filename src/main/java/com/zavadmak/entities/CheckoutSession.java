package com.zavadmak.entities;

import jakarta.persistence.*;

import java.sql.Timestamp;

@Entity
public class CheckoutSession {

    public CheckoutSession() {}
    public CheckoutSession(Checkout checkout, Seller seller, Timestamp startedAt) {
        this.checkout = checkout;
        this.seller = seller;
        this.startedAt = startedAt;
    }

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "sessionId")
    private int sessionId;

    public int getSessionId() {
        return sessionId;
    }

    public void setSessionId(int sessionId) {
        this.sessionId = sessionId;
    }

    @ManyToOne
    @JoinColumn(name = "sellerId")
    private Seller seller;

    public Seller getSeller() {
        return seller;
    }

    public void setSeller(Seller seller) {
        this.seller = seller;
    }

    @ManyToOne
    @JoinColumn(name = "checkoutId")
    private Checkout checkout;

    public Checkout getCheckout() {
        return checkout;
    }

    public void setCheckout(Checkout checkout) {
        this.checkout = checkout;
    }

    @Basic
    @Column(name = "startedAt")
    private Timestamp startedAt;

    public Timestamp getStartedAt() {
        return startedAt;
    }

    public void setStartedAt(Timestamp startedAt) {
        this.startedAt = startedAt;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CheckoutSession that = (CheckoutSession) o;

        if (sessionId != that.sessionId) return false;
        if (startedAt != null ? !startedAt.equals(that.startedAt) : that.startedAt != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = sessionId;
        result = 31 * result + (startedAt != null ? startedAt.hashCode() : 0);
        return result;
    }
}
