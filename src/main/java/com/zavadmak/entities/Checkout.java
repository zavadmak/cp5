package com.zavadmak.entities;

import jakarta.persistence.*;

import java.sql.Timestamp;

@Entity
public class Checkout {

    public Checkout() {
    }

    public Checkout(String manufactureNumber, String localNumber, Manager manager, Timestamp createdAt) {
        this.manufactureNumber = manufactureNumber;
        this.localNumber = localNumber;
        this.manager = manager;
        this.createdAt = createdAt;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "checkoutid")
    private int checkoutId;

    public int getCheckoutId() {
        return checkoutId;
    }

    public void setCheckoutId(int checkoutId) {
        this.checkoutId = checkoutId;
    }

    @Basic
    @Column(name = "manufacturenumber", updatable = false, nullable = false)
    private String manufactureNumber;

    public String getManufactureNumber() {
        return manufactureNumber;
    }

    public void setManufactureNumber(String manufactureNumber) {
        this.manufactureNumber = manufactureNumber;
    }

    @Basic
    @Column(name = "localnumber")
    private String localNumber;

    public String getLocalNumber() {
        return localNumber;
    }

    public void setLocalNumber(String localNumber) {
        this.localNumber = localNumber;
    }

    @ManyToOne(targetEntity = Manager.class)
    @JoinColumn(name = "managerId")
    private Manager manager;

    public Manager getManager() {
        return manager;
    }

    public void setManager(Manager manager) {
        this.manager = manager;
    }

    @Basic
    @Column(name = "createdat")
    private Timestamp createdAt;

    public Timestamp getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Timestamp createdAt) {
        this.createdAt = createdAt;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Checkout checkout = (Checkout) o;

        if (checkoutId != checkout.checkoutId) return false;
        if (manufactureNumber != null ? !manufactureNumber.equals(checkout.manufactureNumber) : checkout.manufactureNumber != null)
            return false;
        if (localNumber != null ? !localNumber.equals(checkout.localNumber) : checkout.localNumber != null)
            return false;
        if (createdAt != null ? !createdAt.equals(checkout.createdAt) : checkout.createdAt != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = checkoutId;
        result = 31 * result + (manufactureNumber != null ? manufactureNumber.hashCode() : 0);
        result = 31 * result + (localNumber != null ? localNumber.hashCode() : 0);
        result = 31 * result + (createdAt != null ? createdAt.hashCode() : 0);
        return result;
    }
}
