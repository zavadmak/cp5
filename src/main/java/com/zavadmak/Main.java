package com.zavadmak;

import com.zavadmak.entities.Checkout;
import com.zavadmak.entities.Manager;
import com.zavadmak.entities.Product;
import com.zavadmak.entities.Seller;
import com.zavadmak.services.*;
import jakarta.persistence.EntityManager;
import jakarta.persistence.Persistence;

import java.util.List;
import java.util.Random;
import java.util.logging.Logger;

public class Main {
    private static final Logger LOGGER = Logger.getLogger(Main.class.getName());

    public static void main(String[] args) throws Exception {
        cleanup();

        // Create manufacturers
        createManufacturers();
        // Create products
        createProducts();
        // Show products
        LOGGER.info("showProducts ...");
        showProducts();
        updateRandomProductDescription();
        LOGGER.info("showProducts ...");
        showProducts();
        // Create managers
        createManagers();
        // Create checkouts
        createCheckouts();
        // Show checkouts
        LOGGER.info("showCheckouts ...");
        showCheckouts();
        createSellers();
        LOGGER.info("showSellers ...");
        showSellers();
        createSessions();
        LOGGER.info("showSessions ...");
        showSessions();
        removeRandomSellers();
        LOGGER.info("showSessions ...");
        showSessions();
        LOGGER.info("showSellers ...");
        showSellers();
    }

    private static void createManufacturers() throws Exception {

        ManufacturerService manService = ManufacturerService.getInstance();
        manService.create("SIT");
        manService.create("OI");
        manService.create("KYR");
    }

    private static void createProducts() throws Exception {
        ProductService productService = ProductService.getInstance();
        ManufacturerService manService = ManufacturerService.getInstance();

        productService.create("Popcorn", "The popcorn desc", manService.getAll());
        productService.create("Kiwi", "The kiwi description", manService.getAll().subList(0, 1));
    }

    private static void updateRandomProductDescription() {
        ProductService productService = ProductService.getInstance();
        List<Product> products = productService.getAll();
        try {
            Random r = new Random();
            Product p = products.get(r.nextInt(products.size() - 1));
            productService.updateDescription(p.getProductId(), "The updated description");
        } catch (Exception e) {
            LOGGER.warning(e.getMessage());
        }
    }

    private static void showProducts() {
        ProductService productService = ProductService.getInstance();
        productService.getAll().forEach((p) -> {
            System.out.println("Product: " + p.getProductName());
            System.out.println("Manufacturers: ");
            p.getManufacturers().forEach((m) -> {
                System.out.println(m.getCompanyName());
            });
        });
    }

    private static void createManagers() throws Exception {
        ManagerService service = ManagerService.getInstance();
//        service.create("Temp User", "9999-12-12", "temp2", "123", "fel");
        service.create("Maksym Zavada", "2005-02-12", "zavadmak", "123", "fel");
        service.create("Brnensky Papousek", "2024-05-24", "papbrno", "123", "vut");
        service.getAll().forEach((m) -> {
            System.out.println(m.getFullName());
        });
        Manager m = service.getAll().get(0);
        m.getCheckouts().forEach((e) -> {
            System.out.println(e.getManufactureNumber() + ": added by " + e.getManager().getFullName());
        });
    }

    private static void createCheckouts() throws Exception {
        CheckoutService service = CheckoutService.getInstance();
        service.create("MN 21244", "122", getManagerCreateIfNotExists());
        // Create some sellers
        for (int i = 1; i <= 10; i++) {
            try {
                service.create("MN " + i, String.valueOf(i), getManagerCreateIfNotExists());
            } catch (Exception e) {
                continue;
            }
        }
    }

    private static void showCheckouts() {
        CheckoutService service = CheckoutService.getInstance();
        // Find all sellers and print
        service.getAll().forEach((c) -> {
            System.out.println(c.getLocalNumber() + ": " + c.getManufactureNumber() + ", " + c.getManager().getFullName());
        });
    }

    private static void createSellers() throws Exception {
        SellerService service = SellerService.getInstance();
        // Create some sellers
        for (int i = 1; i <= 10; i++) {
            try {
                service.create("Seller " + i, "2024-05-" + i, "sel" + i, "123", "1-800-800-8000");
            } catch (Exception e) {
                continue;
            }
        }
    }

    private static void showSellers() {
        SellerService service = SellerService.getInstance();
        // Find all sellers and print
        service.getAll().forEach((s) -> {
            System.out.println(s.getLogin() + ": " + s.getFullName() + ", " + s.getBirthday());
        });
    }

    private static void removeRandomSellers() throws Exception {
        Random random = new Random();
        SellerService service = SellerService.getInstance();
        List<Seller> sellers = service.getAll();
        if (!sellers.isEmpty()) {
            // Remove random seller
            service.removeById(sellers.get(random.nextInt(sellers.size() - 1)).getUserId());
        }
    }

    private static void createSessions() throws Exception {
        Random random = new Random();
        List<Seller> sellers = SellerService.getInstance().getAll();
        List<Checkout> checkouts = CheckoutService.getInstance().getAll();
        CheckoutSessionService service = CheckoutSessionService.getInstance();
        int tryCreateSellers = 0;
        while (sellers.isEmpty()) {
            if (tryCreateSellers > 2) {
                LOGGER.info("Cannot create sellers");
                return;
            }
            tryCreateSellers++;
        }

        int tryCreateCheckouts = 0;
        while (checkouts.isEmpty()) {
            if (tryCreateCheckouts > 2) {
                LOGGER.info("Cannot create checkouts");
                return;
            }
            tryCreateCheckouts++;
        }

        try {
            service.create(
                    checkouts.get(random.nextInt(checkouts.size() - 1)),
                    sellers.get(random.nextInt(sellers.size() - 1)));
        } catch (Exception e) {
            LOGGER.info(e.getMessage());
        }
    }

    private static void showSessions() {
        CheckoutSessionService service = CheckoutSessionService.getInstance();
        // Show all sessions
        service.getAll().forEach((session) -> {
            System.out.println(session.getSessionId() + ": " + session.getCheckout().getManufactureNumber() + ", managed by " + session.getSeller().getFullName());
        });
    }

    public static Manager getManagerCreateIfNotExists() {
        List<Manager> managers = ManagerService.getInstance().getAll();
        if (managers.isEmpty()) {
            try {
                String login = "temp1";
                ManagerService.getInstance().create("Temp User", "9999-12-12", login, "123", "fel");
                return ManagerService.getInstance().getByLogin(login);
            } catch (Exception e) {
                LOGGER.severe("Cannot create the manager");
            }
        }
        return managers.get(managers.size() - 1);
    }

    public static void cleanup() {
        EntityManager em = Persistence.createEntityManagerFactory("default").createEntityManager();
        // sessions
        CheckoutSessionService.getInstance().getAll().forEach(e -> {
            em.getTransaction().begin();
            em.remove(em.merge(e));
            em.getTransaction().commit();
        });

        // checkouts
        CheckoutService.getInstance().getAll().forEach(e -> {
            em.getTransaction().begin();
            em.remove(em.merge(e));
            em.getTransaction().commit();
        });

        // sellers
        SellerService.getInstance().getAll().forEach(e -> {
            em.getTransaction().begin();
            em.remove(em.merge(e));
            em.getTransaction().commit();
        });

        // managers
        ManagerService.getInstance().getAll().forEach(e -> {
            em.getTransaction().begin();
            em.remove(em.merge(e));
            em.getTransaction().commit();
        });

        // products
        ProductService.getInstance().getAll().forEach(e -> {
            em.getTransaction().begin();
            em.remove(em.merge(e));
            em.getTransaction().commit();
        });

        // Cleanup manufacturers
        ManufacturerService.getInstance().getAll().forEach(e -> {
            em.getTransaction().begin();
            em.remove(em.merge(e));
            em.getTransaction().commit();
        });
    }
}
