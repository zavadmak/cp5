package com.zavadmak.services;

import com.zavadmak.entities.Checkout;
import com.zavadmak.entities.Manager;
import com.zavadmak.exceptions.ServiceException;
import com.zavadmak.exceptions.NotFoundException;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.Persistence;
import jakarta.persistence.TypedQuery;

import java.sql.Timestamp;
import java.util.List;

/**
 * The singleton checkout service
 */
public class CheckoutService {
    private final EntityManagerFactory emf;

    private CheckoutService() {
        emf = Persistence.createEntityManagerFactory("default");
    }

    private static CheckoutService instance = null;

    public static CheckoutService getInstance() {
        if (instance == null)
            instance = new CheckoutService();
        return instance;
    }

    /**
     * Create a new checkout
     */
    public void create(String manufactureNumber, String localNumber, Manager manager) throws ServiceException {
        EntityManager em = emf.createEntityManager();
        try {
            em.getTransaction().begin();
            em.persist(new Checkout(manufactureNumber, localNumber, manager, new Timestamp(System.currentTimeMillis())));
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
            throw new ServiceException("The Checkout already exists");
        }
    }

    /**
     * Get checkout by manufacture number
     *
     * @param manufactureNumber The manufacture number
     */
    public Checkout getByManufactureNumber(String manufactureNumber) throws NotFoundException {
        EntityManager em = emf.createEntityManager();
        TypedQuery<Checkout> query = em.createQuery("SELECT c FROM Checkout as c WHERE c.manufactureNumber = :num", Checkout.class)
                .setParameter("num", manufactureNumber);
        if (query.getResultList().isEmpty())
            throw new NotFoundException("Checkout not found");
        return query.getSingleResult();
    }

    /**
     * Get checkout by id
     *
     * @param id The checkout id
     */
    public Checkout getById(Integer id) throws NotFoundException {
        EntityManager em = emf.createEntityManager();
        TypedQuery<Checkout> query = em.createQuery("SELECT c FROM Checkout as c WHERE c.checkoutId = :id", Checkout.class)
                .setParameter("id", id);
        if (query.getResultList().isEmpty())
            throw new NotFoundException("Checkout not found");
        return query.getSingleResult();
    }

    /**
     * Get number of the checkout on the page
     * @param limit The limit. If the limit is 0, the limit keyword will not be use
     * @param page The page started from 1
     */
    public List<Checkout> getAll(Integer limit, Integer page) {
        EntityManager em = emf.createEntityManager();
        TypedQuery<Checkout> query = em.createQuery("SELECT m FROM Checkout as m", Checkout.class);
        if (limit > 0 && page >= 0)
            query.setMaxResults(limit)
                    .setFirstResult(limit * page);
        else if (limit > 0)
            query.setMaxResults(limit);
        return query.getResultList();
    }

    /**
     * Get number of the checkout
     * @param limit The limit
     */
    public List<Checkout> getAll(Integer limit) {
        return getAll(limit, 0);
    }

    /**
     * Get all checkouts
     */
    public List<Checkout> getAll() {
        return getAll(0);
    }
}
