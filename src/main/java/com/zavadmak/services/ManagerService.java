package com.zavadmak.services;

import com.zavadmak.entities.Manager;
import com.zavadmak.exceptions.ServiceException;
import com.zavadmak.exceptions.NotFoundException;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.Persistence;
import jakarta.persistence.TypedQuery;

import java.util.List;

/**
 * The manager service singleton
 */
public class ManagerService {
    private final EntityManagerFactory emf;

    private ManagerService() {
        emf = Persistence.createEntityManagerFactory("default");
    }

    private static ManagerService instance = null;

    public static ManagerService getInstance() {
        if (instance == null)
            instance = new ManagerService();
        return instance;
    }

    /**
     * Create the manager
     * @param fullName The full name
     * @param birthday The birthday
     * @param login The login
     * @param password The password
     * @param education The education
     * @throws ServiceException If the same manager already exists
     */
    public void create(String fullName, String birthday, String login, String password, String education) throws ServiceException {
        EntityManager em = emf.createEntityManager();
        try {
            em.getTransaction().begin();
            em.persist(new Manager(fullName, birthday, login, password, education));
            em.getTransaction().commit();
        } catch (Exception e) {
            throw new ServiceException("Manager already exists");
        }
    }

    /**
     * Get number of the managers on the page
     *
     * @param limit The limit. If the limit is 0, the limit keyword will not be use
     * @param page  The page started from 1
     */
    public List<Manager> getAll(Integer limit, Integer page) {
        EntityManager em = emf.createEntityManager();
        TypedQuery<Manager> query = em.createQuery("SELECT m FROM Manager as m", Manager.class);
        if (limit > 0 && page >= 0)
            query.setMaxResults(limit)
                    .setFirstResult(limit * page);
        else if (limit > 0)
            query.setMaxResults(limit);
        return query.getResultList();
    }

    /**
     * Get number of the managers
     *
     * @param limit The limit
     */
    public List<Manager> getAll(Integer limit) {
        return getAll(limit, 0);
    }

    /**
     * Get all managers
     */
    public List<Manager> getAll() {
        return getAll(0);
    }

    /**
     * Get manager by id
     *
     * @param id The user id
     */
    public Manager getById(Integer id) throws NotFoundException {
        EntityManager em = emf.createEntityManager();
        TypedQuery<Manager> query = em.createQuery("SELECT m FROM Manager as m WHERE m.userId = :id", Manager.class)
                .setParameter("id", id);
        if (query.getResultList().isEmpty()) {
            throw new NotFoundException("Manager not found");
        }
        return query.getSingleResult();
    }

    /**
     * Get manager by login
     *
     * @param login The login
     */
    public Manager getByLogin(String login) throws NotFoundException {
        EntityManager em = emf.createEntityManager();
        TypedQuery<Manager> query = em.createQuery("SELECT m FROM Manager as m WHERE m.login = :login", Manager.class)
                .setParameter("login", login);
        if (query.getResultList().isEmpty()) {
            throw new NotFoundException("Manager not found");
        }
        return query.getSingleResult();
    }

    /**
     * Remove user by id
     *
     * @param id The user id
     * @throws NotFoundException If user not found
     */
    public void removeById(Integer id) throws NotFoundException {
        EntityManager em = emf.createEntityManager();
        Manager candidate = getById(id);
        em.getTransaction().begin();
        if (!em.contains(candidate)) {
            candidate = em.merge(candidate);
        }
        em.remove(candidate);
        em.getTransaction().commit();
        em.close();
    }
}
