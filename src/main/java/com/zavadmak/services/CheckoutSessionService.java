package com.zavadmak.services;

import com.zavadmak.entities.Checkout;
import com.zavadmak.entities.CheckoutSession;
import com.zavadmak.entities.Seller;
import com.zavadmak.exceptions.ServiceException;
import com.zavadmak.exceptions.NotFoundException;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.Persistence;
import jakarta.persistence.TypedQuery;

import java.sql.Timestamp;
import java.util.List;


/**
 * Checkout session singleton service
 */
public class CheckoutSessionService {
    private final EntityManagerFactory emf;

    private CheckoutSessionService() {
        emf = Persistence.createEntityManagerFactory("default");
    }

    private static CheckoutSessionService instance;

    public static CheckoutSessionService getInstance() {
        if (instance == null)
            instance = new CheckoutSessionService();
        return instance;
    }

    /**
     * Create a new checkout session
     */
    public void create(Checkout checkout, Seller seller) throws ServiceException {
        EntityManager em = emf.createEntityManager();
        try {
            em.getTransaction().begin();
            em.persist(new CheckoutSession(checkout, seller, new Timestamp(System.currentTimeMillis())));
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
            throw new ServiceException("The Checkout already exists");
        }
    }

    /**
     * Get checkout by id
     *
     * @param id The checkout id
     */
    public CheckoutSession getById(Integer id) throws NotFoundException {
        EntityManager em = emf.createEntityManager();
        TypedQuery<CheckoutSession> query = em.createQuery("SELECT c FROM CheckoutSession as c WHERE c.sessionId = :id", CheckoutSession.class)
                .setParameter("id", id);
        if (query.getResultList().isEmpty())
            throw new NotFoundException("Checkout session not found");
        return query.getSingleResult();
    }

    /**
     * Get number of the checkout session on the page
     *
     * @param limit The limit. If the limit is 0, the limit keyword will not be use
     * @param page  The page started from 1
     */
    public List<CheckoutSession> getAll(Integer limit, Integer page) {
        EntityManager em = emf.createEntityManager();
        TypedQuery<CheckoutSession> query = em.createQuery("SELECT m FROM CheckoutSession as m", CheckoutSession.class);
        if (limit > 0 && page >= 0)
            query.setMaxResults(limit)
                    .setFirstResult(limit * page);
        else if (limit > 0)
            query.setMaxResults(limit);
        return query.getResultList();
    }

    /**
     * Get number of the checkout session
     *
     * @param limit The limit
     */
    public List<CheckoutSession> getAll(Integer limit) {
        return getAll(limit, 0);
    }

    /**
     * Get all checkouts
     */
    public List<CheckoutSession> getAll() {
        return getAll(0);
    }
}
