package com.zavadmak.services;

import com.zavadmak.entities.Seller;
import com.zavadmak.exceptions.ServiceException;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.Persistence;
import jakarta.persistence.TypedQuery;

import java.util.List;

/**
 * Seller singleton service
 */
public class SellerService {
    private final EntityManagerFactory emf;

    private SellerService() {
        emf = Persistence.createEntityManagerFactory("default");
    }

    private static SellerService instance;

    public static SellerService getInstance() {
        if (instance == null)
            instance = new SellerService();
        return instance;
    }

    /**
     * Create new seller
     */
    public void create(String fullName, String birthday, String login, String password, String internalPhone) throws ServiceException {
        EntityManager em = emf.createEntityManager();
        try {
            em.getTransaction().begin();
            em.persist(new Seller(fullName, birthday, login, password, internalPhone));
            em.getTransaction().commit();
        } catch (Exception e) {
            throw new ServiceException("Seller already exists");
        }
    }

    /**
     * Get number of the sellers on the page
     * @param limit The limit. If the limit is 0, the limit keyword will not be use
     * @param page The page started from 1
     */
    public List<Seller> getAll(Integer limit, Integer page) {
        EntityManager em = emf.createEntityManager();
        TypedQuery<Seller> query = em.createQuery("SELECT m FROM Seller as m", Seller.class);
        if (limit > 0 && page >= 0)
            query.setMaxResults(limit)
                    .setFirstResult(limit * page);
        else if (limit > 0)
            query.setMaxResults(limit);
        return query.getResultList();
    }

    /**
     * Get number of the sellers
     * @param limit The limit
     */
    public List<Seller> getAll(Integer limit) {
        return getAll(limit, 0);
    }

    /**
     * Get all sellers
     */
    public List<Seller> getAll() {
        return getAll(0);
    }

    /**
     * Get seller by id
     * @param id The user id
     */
    public Seller getById(Integer id){
        EntityManager em = emf.createEntityManager();
        return em.createQuery("SELECT m FROM Seller as m WHERE m.userId = :id", Seller.class)
                .setParameter("id", id)
                .getSingleResult();
    }

    public void removeById(Integer i) throws Exception {
        EntityManager em = emf.createEntityManager();
        try {
            em.getTransaction().begin();
            Seller s = em.find(Seller.class, i);
            em.remove(s);
            em.getTransaction().commit();
        } catch (Exception e) {
            throw e;
        }
    }
}
