package com.zavadmak.services;

import com.zavadmak.entities.Storekeeper;
import com.zavadmak.exceptions.ServiceException;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.Persistence;
import jakarta.persistence.TypedQuery;

import java.util.List;

public class StorekeeperService {
    private final EntityManagerFactory emf;

    public StorekeeperService() {
        emf = Persistence.createEntityManagerFactory("default");
    }

    /**
     * Create new storekeeper
     */
    public void create(String fullName, String birthday, String login, String password, String position) throws ServiceException {
        EntityManager em = emf.createEntityManager();
        try {
            em.getTransaction().begin();
            em.persist(new Storekeeper(fullName, birthday, login, password, position));
            em.getTransaction().commit();
        } catch (Exception e) {
            throw new ServiceException("Storekeeper already exists");
        }
    }

    /**
     * Get number of the customers on the page
     * @param limit The limit. If the limit is 0, the limit keyword will not be use
     * @param page The page started from 1
     */
    public List<Storekeeper> getAll(Integer limit, Integer page) {
        EntityManager em = emf.createEntityManager();
        TypedQuery<Storekeeper> query = em.createQuery("SELECT m FROM Storekeeper as m", Storekeeper.class);
        if (limit > 0 && page >= 0)
            query.setMaxResults(limit)
                    .setFirstResult(limit * page);
        else if (limit > 0)
            query.setMaxResults(limit);
        return query.getResultList();
    }

    /**
     * Get number of the managers
     * @param limit The limit
     */
    public List<Storekeeper> getAll(Integer limit) {
        return getAll(limit, 0);
    }

    /**
     * Get all managers
     */
    public List<Storekeeper> getAll() {
        return getAll(0);
    }

    /**
     * Get manager by id
     * @param id The user id
     */
    public Storekeeper getById(Integer id){
        EntityManager em = emf.createEntityManager();
        return em.createQuery("SELECT m FROM Storekeeper as m WHERE m.userId = :id", Storekeeper.class)
                .setParameter("id", id)
                .getSingleResult();
    }
}
