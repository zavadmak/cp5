package com.zavadmak.services;

import com.zavadmak.entities.Manufacturer;
import com.zavadmak.entities.Product;
import com.zavadmak.exceptions.ServiceException;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.Persistence;

import java.util.List;

public class ProductService {
    private final EntityManagerFactory emf;

    private ProductService() {
        emf = Persistence.createEntityManagerFactory("default");
    }

    private static ProductService instance;

    public static ProductService getInstance() {
        if (instance == null)
            instance = new ProductService();
        return instance;
    }

    public void create(String name, String description, List<Manufacturer> manufacturers) throws ServiceException {
        EntityManager em = emf.createEntityManager();
        try {
            em.getTransaction().begin();
            em.persist(new Product(name, description, manufacturers));
            em.getTransaction().commit();
        } catch (Exception e) {
            throw new ServiceException("Product already exists");
        }
    }

    public void updateDescription(Integer id, String newDescription) throws Exception {
        EntityManager em = emf.createEntityManager();
        try {
            em.getTransaction().begin();
            Product p = getById(id);
            p.setProductDescription(newDescription);
            em.merge(p);
            em.getTransaction().commit();
        } catch (Exception e) {
            throw new ServiceException("Product already exists");
        }
    }

    public Product getById(Integer id) {
        EntityManager em = emf.createEntityManager();
        return em.find(Product.class, id);
    }

    public List<Product> getAll() {
        EntityManager em = emf.createEntityManager();
        return em.createQuery("SELECT p FROM Product as p", Product.class).getResultList();
    }

    public void remove(Product p) {
        EntityManager em = emf.createEntityManager();
        em.remove(p);
    }
}
