package com.zavadmak.services;

import com.zavadmak.entities.Customer;
import com.zavadmak.exceptions.ServiceException;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.Persistence;
import jakarta.persistence.TypedQuery;

import java.util.List;

public class CustomerService {
    private final EntityManagerFactory emf;

    public CustomerService(){
        emf = Persistence.createEntityManagerFactory("default");
    }

    /**
     * Create new customer
     */
    public void create(String fullName, String birthday, String login, String password, Integer psc, String town, String street) throws ServiceException {
        EntityManager em = emf.createEntityManager();
        try {
            em.getTransaction().begin();
            em.persist(new Customer(fullName, birthday, login, password, psc, town, street));
            em.getTransaction().commit();
        } catch (Exception e) {
            throw new ServiceException("Customer already exists");
        }
    }

    /**
     * Get number of the customers on the page
     * @param limit The limit. If the limit is 0, the limit keyword will not be use
     * @param page The page started from 1
     */
    public List<Customer> getAll(Integer limit, Integer page) {
        EntityManager em = emf.createEntityManager();
        TypedQuery<Customer> query = em.createQuery("SELECT m FROM Customer as m", Customer.class);
        if (limit > 0 && page >= 0)
            query.setMaxResults(limit)
                    .setFirstResult(limit * page);
        else if (limit > 0)
            query.setMaxResults(limit);
        return query.getResultList();
    }

    /**
     * Get number of the managers
     * @param limit The limit
     */
    public List<Customer> getAll(Integer limit) {
        return getAll(limit, 0);
    }

    /**
     * Get all managers
     */
    public List<Customer> getAll() {
        return getAll(0);
    }

    /**
     * Get manager by id
     * @param id The user id
     */
    public Customer getById(Integer id){
        EntityManager em = emf.createEntityManager();
        return em.createQuery("SELECT m FROM Customer as m WHERE m.userId = :id", Customer.class)
                .setParameter("id", id)
                .getSingleResult();
    }
}
