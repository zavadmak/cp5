package com.zavadmak.services;

import com.zavadmak.entities.Manufacturer;
import com.zavadmak.exceptions.ServiceException;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.Persistence;

import java.util.List;

public class ManufacturerService {
    private final EntityManagerFactory emf;

    private ManufacturerService() {
        emf = Persistence.createEntityManagerFactory("default");
    }

    private static ManufacturerService instance;

    public static ManufacturerService getInstance() {
        if (instance == null)
            instance = new ManufacturerService();
        return instance;
    }

    public void create(String companyName) throws ServiceException {
        EntityManager em = emf.createEntityManager();
        try {
            em.getTransaction().begin();
            em.persist(new Manufacturer(companyName));
            em.getTransaction().commit();
        } catch (Exception e) {
            throw new ServiceException("Manufacturer already exists");
        }
    }

    public Manufacturer getById(Integer id) {
        EntityManager em = emf.createEntityManager();
        return em.find(Manufacturer.class, id);
    }

    public List<Manufacturer> getAll() {
        EntityManager em = emf.createEntityManager();
        return em.createQuery("SELECT m FROM Manufacturer as m", Manufacturer.class).getResultList();
    }
}
