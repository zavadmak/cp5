package com.zavadmak.utils;

import java.sql.Date;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public class DateParser {
    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd");

    private static final SimpleDateFormat DATE_TIME_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    public static Date dateOf(String s) {
        try {
            return new Date(DATE_FORMAT.parse(s).getTime());
        } catch (ParseException e) {
            throw new IllegalArgumentException(e);
        }
    }

    public static Timestamp timestampOf(String s) {
        try {
            return new Timestamp(DATE_TIME_FORMAT.parse(s).getTime());
        } catch (ParseException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
