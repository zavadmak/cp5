package com.zavadmak.utils;

import java.util.HashMap;
import java.util.StringJoiner;

public class ValidationResult {
    private final String subject;
    private final HashMap<String, String> result = new HashMap<>();

    public ValidationResult(String subject) {
        this.subject = subject;
    }

    public boolean isValid() {
        return result.isEmpty();
    }

    public boolean isNotValid() {
        return !isValid();
    }

    public void addResult(String key, String reason) {
        result.put(key, reason);
    }

    public String getSubject() {
        return subject;
    }

    public HashMap<String, String> getResult() {
        return result;
    }

    @Override
    public String toString() {
        StringJoiner sj = new StringJoiner(System.lineSeparator());
        result.forEach((k, v) -> {
            sj.add(k + ": " + v);

        });
        return "The " + subject + " has following result: " + System.lineSeparator() + sj;
    }
}
