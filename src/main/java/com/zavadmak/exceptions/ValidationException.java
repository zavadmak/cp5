package com.zavadmak.exceptions;

public class ValidationException extends Exception {
    public ValidationException(String m) {
        super(m);
    }
}
