package com.zavadmak.exceptions;

public class NotFoundException extends Exception{
    public NotFoundException(String m) {
        super(m);
    }
}
