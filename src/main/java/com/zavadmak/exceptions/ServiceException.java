package com.zavadmak.exceptions;

public class ServiceException extends Exception {
    public ServiceException(String m) {
        super(m);
    }
}
