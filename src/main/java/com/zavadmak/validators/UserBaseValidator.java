package com.zavadmak.validators;

import com.zavadmak.entities.UserBase;
import com.zavadmak.utils.ValidationResult;

public class UserBaseValidator {
    public static ValidationResult validate(UserBase u) {
        ValidationResult result = new ValidationResult("Userbase");
        if (u.getFullName() == null)
            result.addResult("Fullname", "is empty");
        if (u.getLogin() == null)
            result.addResult("Login", "is empty");
        if (u.getBirthday() == null)
            result.addResult("Birthday", "is empty");
        if (u.getPassword() == null)
            result.addResult("Password", "is empty");
        return result;
    }
}
